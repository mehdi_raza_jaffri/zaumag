=== smart Custom Display Name ===
Contributors: petersplugins, smartware.cc
Donate link: http://petersplugins.com/make-a-donation/
Tags: author, authors, user, users, name, display name, profile, user-profile, custom name, user name, author name, the_author, get_the_author, the_author_link, get_the_author_link, the_author_posts_link, wp_list_authors
Requires at least: 3.0
Tested up to: 4.7
Stable tag: 1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Allows you to change the value of "Display name publicly as" in user profiles to any string

== Description ==

> Set your Display Name to anything you like!

See also [Plugin Homepage](http://petersplugins.com/free-wordpress-plugins/smart-custom-display-name/)

The "Display Name" is the name shown as post author - e.g. "posted by John Doe".

WordPress only allows you to select from these given values:

* Nickname - e.g. "JoDo"
* Username (Login) - e.g. "J0hnD03"
* First Name - e.g. "John"
* Last Name - e.g. "Doe"
* First + Last - e.g. "John Doe"
* Last + First - e.g. "Doe John"

It is not possible to set the Display Name to anything else you want to be displayed - like e.g. "John - the JoDo - Doe".

For real? OK, indeed it is possible - with this simple and handy plugin. There are no settings. Just install and activate. Display Name field becomes a regular text input field where you can type in anything you like.

= Do you like the smart Custom Display Name Plugin? =

Thanks, I appreciate that. You don't need to make a donation. No money, no beer, no coffee. Please, just [tell the world that you like what I'm doing](http://petersplugins.com/make-a-donation/)! And that's all.

= More plugins from Peter =

* **[404page](https://wordpress.org/plugins/404page/)** - Define any of your WordPress pages as 404 error page 
* **[hashtagger](https://wordpress.org/plugins/hashtagger/)** - Use hashtags in WordPress
* **[smart User Slug Hider](https://wordpress.org/plugins/smart-user-slug-hider/)** - Hide usernames in author pages URLs to enhance security 
* [See all](https://profiles.wordpress.org/petersplugins/#content-plugins)

== Installation ==

= From your WordPress dashboard =

1. Visit 'Plugins' -> 'Add New'
1. Search for 'smart Custom Display Name'
1. Activate the plugin through the 'Plugins' menu in WordPress

= Manually from wordpress.org =

1. Download smart Custom Display Name from wordpress.org and unzip the archive
1. Upload the `smart-custom-display-name` folder to your `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= What about an empty Display name? =

WordPress does not allow an empty Display name and resets the Display name to the Username on saving if its blank. You don't have to care about.

= Where can I find the settings page? =

Nowhere - the plugin does not need any config.

== Screenshots ==

1. Original WordPress User Profile
2. User Profile with activated smart Custom Display Name plugin

== Changelog ==

= 1.2 (2016-10-11) =
* Some changes under the hood, no functional changes

= 1.1 (2016-06-23) =
* Code optimization, some UI work, no functional changes

= 1.0 (2015-08-31) =
* Initial Release

== Upgrade Notice ==

= 1.2 =
Some changes under the hood, no functional changes

= 1.1 =
Code optimization, some UI work, no functional changes