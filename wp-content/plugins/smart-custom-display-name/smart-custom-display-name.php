<?php
/*
Plugin Name: smart Custom Display Name
Plugin URI: http://petersplugins.com/free-wordpress-plugins/smart-custom-display-name/
Description: Set users "Display Name" to any custom value
Version: 1.2
Author: Peter's Plugins, smartware.cc
Author URI: http://petersplugins.com
Text Domain: smart-custom-display-name
License: GPL2+
License URI: http://www.gnu.org/licenses/gpl-2.0.txt
*/

if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once( plugin_dir_path( __FILE__ ) . '/inc/class-smart-custom-display-name.php' );

$smart_custom_display_name = new Smart_Custom_Display_Name( __FILE__ );

?>