<?php

if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( !class_exists( 'Smart_Custom_Display_Name' ) ) { 

  class Smart_Custom_Display_Name {
    
    private $_file;
    public $plugin_name;
    public $plugin_slug;
    public $version;
    private $wp_url;
    private $my_url;
    private $dc_url;
    
    public function __construct( $file ) {
      $this->_file = $file;
      $this->plugin_name = 'smart Custom Display Name';
      $this->plugin_slug = 'smart-custom-display-name';
      $this->version = '1.2';
      $this->init();
    }
    
    function init() {
      if ( is_admin() ) {
        add_action( 'admin_init', array( $this, 'admin_init' ) );
        add_action( 'admin_menu', array( $this, 'add_info_page' ) );
        add_filter( 'plugin_action_links_' . plugin_basename( $this->_file ), array( $this, 'add_link' ) ); 
        add_action( 'admin_footer-user-edit.php', array( $this, 'display_name_customizer_js' ) );
        add_action( 'admin_footer-profile.php', array( $this, 'display_name_customizer_js' ) );
        add_action( 'admin_head-user-edit.php', array( $this, 'display_name_customizer_css' ) );
        add_action( 'admin_head-profile.php', array( $this, 'display_name_customizer_css' ) );
      }
    }

    function admin_init() {
      $this->wp_url = 'https://wordpress.org/plugins/' . $this->plugin_slug;
      $this->my_url = 'http://petersplugins.com/free-wordpress-plugins/' . $this->plugin_slug;
      $this->dc_url = 'http://petersplugins.com/docs/' . $this->plugin_slug;
      load_plugin_textdomain( 'smart-custom-display-name' );
    }
    
    function display_name_customizer_js() {
      ?>
      <script type="text/javascript">
        jQuery( document ).ready( function() { jQuery( '#display_name' ).parent().html( '<input type="text" name="display_name" id="display_name" value="' + jQuery( '#display_name' ).val() + '" class="regular-text" />' ); jQuery( '#first_name, #last_name, #nickname' ).unbind( 'blur' ); jQuery( 'label[for="display_name"]' ).html( jQuery( 'label[for="display_name"]' ).html() + '&nbsp;<a class="dashicons dashicons-editor-help" href="<?php menu_page_url( $this->plugin_slug ); ?>"></a>' ); });
      </script>
      <?php
    }
    
    function display_name_customizer_css() {
      ?>
      <style type="text/css">
        #display_name { width: 25em; } @media screen and ( max-width: 782px ) { #display_name { width: 100%; } }
      </style>
      <?php
    }
    
    function add_link( $links ) {
      return array_merge( $links, array( '<a class="dashicons dashicons-editor-help" href="' . menu_page_url( $this->plugin_slug, false ) . '"></a>', '<a href="https://wordpress.org/support/plugin/' . $this->plugin_slug . '/reviews/">' . __( 'Please rate Plugin', 'smart-custom-display-name' ) .'</a>' ) );
    }
    
    function add_info_page() {
      add_submenu_page( null, $this->plugin_name, $this->plugin_name, 'read', $this->plugin_slug, array( $this, 'show_info_page' ) );
    }
    
    function show_info_page() {
      ?>    
      <div class="wrap">
        <?php screen_icon(); ?>
        <h2 style="min-height: 32px; line-height: 32px; padding-left: 40px; background-image: url(<?php echo plugins_url( 'pluginicon.png', $this->_file ); ?>); background-repeat: no-repeat; background-position: left center"><a href="<?php echo $this->my_url; ?>"><?php echo $this->plugin_name; ?></a></h2>
        <hr />
        <p>Plugin Version: <?php echo $this->version; ?> <a class="dashicons dashicons-editor-help" href="<?php echo $this->wp_url; ?>/changelog/"></a></p>
        <div id="poststuff">
          <div id="post-body" class="metabox-holder columns-2">
            <div id="post-body-content">
              <div class="meta-box-sortables ui-sortable">
                <div class="postbox">
                  <div class="inside">
                    <p><strong><?php _e( 'This plugin allows you to change the users Display Name to anything you like.', 'smart-custom-display-name' ); ?></strong></p>
                    <p><?php _e( 'There are no settings. When activated the plugin changes the "Display name publicly as" field on the user settings page from a select box where you only can choose from maximum 6 possible values to a  regular text input field where you can type in anything you like.', 'smart-custom-display-name' ); ?></p>
                  </div>
                </div>
                <div class="postbox">
                  <div class="inside">
                    <p><strong><?php _e( 'Do you like the smart Custom Display Name Plugin?', 'smart-custom-display-name' ); ?></strong></p>
                    <p><a href="https://profiles.wordpress.org/petersplugins/#content-plugins"><?php _e( 'Please take a look at my other plugins.', 'smart-custom-display-name' ); ?></a></p>
                  </div>
                </div>
              </div>
            </div>
            <?php { $this->show_meta_boxes(); } ?>
          </div>
          <br class="clear">
        </div>    
      </div>
      <?php
    }
    
    // show meta boxes
    function show_meta_boxes() {
      ?>
      <div id="postbox-container-1" class="postbox-container">
        <div class="meta-box-sortables">
          <div class="postbox">
            <h3><span><?php _e( 'Like this Plugin?', 'smart-custom-display-name' ); ?></span></h3>
            <div class="inside">
              <ul>
                <li><div class="dashicons dashicons-wordpress"></div>&nbsp;&nbsp;<a href="<?php echo $this->wp_url; ?>/"><?php _e( 'Please rate the plugin', 'smart-custom-display-name' ); ?></a></li>
                <li><div class="dashicons dashicons-admin-home"></div>&nbsp;&nbsp;<a href="<?php echo $this->my_url; ?>/"><?php _e( 'Plugin homepage', 'smart-custom-display-name'); ?></a></li>
                <li><div class="dashicons dashicons-admin-home"></div>&nbsp;&nbsp;<a href="http://petersplugins.com/"><?php _e( 'Author homepage', 'smart-custom-display-name' );?></a></li>
                <li><div class="dashicons dashicons-googleplus"></div>&nbsp;&nbsp;<a href="http://g.petersplugins.com/"><?php _e( 'Authors Google+ Page', 'smart-custom-display-name' ); ?></a></li>
                <li><div class="dashicons dashicons-facebook-alt"></div>&nbsp;&nbsp;<a href="http://f.petersplugins.com/"><?php _e( 'Authors facebook Page', 'smart-custom-display-name' ); ?></a></li>
              </ul>
            </div>
          </div>
          <div class="postbox">
            <h3><span><?php _e( 'Need help?', 'smart-custom-display-name' ); ?></span></h3>
            <div class="inside">
              <ul>
                <li><div class="dashicons dashicons-book-alt"></div>&nbsp;&nbsp;<a href="<?php echo $this->dc_url; ?>"><?php _e( 'Take a look at the Plugin Doc', 'smart-custom-display-name' ); ?></a></li>
                <li><div class="dashicons dashicons-wordpress"></div>&nbsp;&nbsp;<a href="<?php echo $this->wp_url; ?>/faq/"><?php _e( 'Take a look at the FAQ section', 'smart-custom-display-name' ); ?></a></li>
                <li><div class="dashicons dashicons-wordpress"></div>&nbsp;&nbsp;<a href="http://wordpress.org/support/plugin/<?php echo $this->plugin_slug; ?>/"><?php _e( 'Take a look at the Support section', 'smart-custom-display-name'); ?></a></li>
                <li><div class="dashicons dashicons-admin-comments"></div>&nbsp;&nbsp;<a href="http://petersplugins.com/contact/"><?php _e( 'Feel free to contact the Author', 'smart-custom-display-name' ); ?></a></li>
              </ul>
            </div>
          </div>
          <div class="postbox">
            <h3><span><?php _e( 'Translate this Plugin', 'smart-custom-display-name' ); ?></span></h3>
            <div class="inside">
              <p><?php _e( 'It would be great if you\'d support the smart Custom Display Name Plugin by adding a new translation or keeping an existing one up to date!', 'smart-custom-display-name' ); ?></p>
              <p><a href="https://translate.wordpress.org/projects/wp-plugins/<?php echo $this->plugin_slug; ?>"><?php _e( 'Translate online', 'smart-custom-display-name' ); ?></a></p>
            </div>
          </div>
        </div>
      </div>
      <?php
    }
  }

}